import { deleteMoney, addRemoveClass } from "./functions.js";
import { configAboutGame } from './script.js';

const preloader = document.querySelector('.preloader');
const mainBody = document.querySelector('.main__body');
const about = document.querySelector('.about-main__body')

// Объявляем слушатель событий "клик"
document.addEventListener('click', (e) => {
	let targetElement = e.target;
	if (targetElement.closest('[data-btn="privacy"]') && preloader.classList.contains('_hide')) {
		preloader.classList.remove('_hide');
	}
	if (targetElement.closest('[data-btn="about"]') && !mainBody.classList.contains('_about')) {
		mainBody.classList.add('_about');
	}

	if (targetElement.closest('.preloader__button')) {
		preloader.classList.add('_hide');
	}

	if (targetElement.closest('.main__btn-back') && configAboutGame.stage === 0 && mainBody.classList.contains('_about')) {
		mainBody.classList.remove('_about');
	}

	if (targetElement.closest('.main__btn-back') && configAboutGame.stage !== 0 && mainBody.classList.contains('_about')) {
		about.classList.remove('_info');
		if (configAboutGame.stage === 1) about.classList.remove('_info_1');
		else if (configAboutGame.stage === 2) about.classList.remove('_info_2');
		else if (configAboutGame.stage === 3) about.classList.remove('_info_3');
		document.querySelector('.main__btn-back').classList.remove('_about-lobby');
		setTimeout(() => {
			configAboutGame.stage = 0;
		}, 0);
	}

	if (targetElement.closest('[data-btn="about-1"]')) {
		about.classList.add('_info');
		about.classList.add('_info_2');
		configAboutGame.stage = 2;
		document.querySelector('.main__btn-back').classList.add('_about-lobby');
	}

	if (targetElement.closest('[data-btn="about-2"]')) {
		about.classList.add('_info');
		about.classList.add('_info_1');
		configAboutGame.stage = 1;
		document.querySelector('.main__btn-back').classList.add('_about-lobby');
	}

	if (targetElement.closest('[data-btn="about-3"]')) {
		about.classList.add('_info');
		about.classList.add('_info_3');
		configAboutGame.stage = 3;
		document.querySelector('.main__btn-back').classList.add('_about-lobby');
	}

	if (document.querySelector('.win-box') && document.querySelector('.win-box').classList.contains('_visible') && targetElement.closest('.win-box__button')) {
		document.querySelector('.win-box').classList.remove('_visible');
	}

})