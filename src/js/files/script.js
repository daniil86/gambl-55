import gsap from 'gsap';
import { deleteMoney, noMoney, getRandom, addMoney } from '../files/functions.js';

if (sessionStorage.getItem('money')) {
	if (document.querySelector('.score')) {
		document.querySelectorAll('.score').forEach(el => {
			el.textContent = sessionStorage.getItem('money');
		})
	}
} else {
	sessionStorage.setItem('money', 5000);
	if (document.querySelector('.score')) {
		document.querySelectorAll('.score').forEach(el => {
			el.textContent = sessionStorage.getItem('money');
		})
	}
}

let tl = gsap.timeline({ defaults: { ease: "Power1.easeInOut", duration: 1.5 } });

//========================================================================================================================================================
// Функция присвоения случайного класса анимациии money icon
const anim_items = document.querySelectorAll('.icon-anim img');
function getRandomAnimate() {
	let number = getRandom(0, 3);
	let arr = ['jump', 'scale', 'rotate'];
	let random_item = getRandom(0, anim_items.length);
	anim_items.forEach(el => {
		if (el.classList.contains('_anim-icon-jump')) {
			el.classList.remove('_anim-icon-jump');
		} else if (el.classList.contains('_anim-icon-scale')) {
			el.classList.remove('_anim-icon-scale');
		} else if (el.classList.contains('_anim-icon-rotate')) {
			el.classList.remove('_anim-icon-rotate');
		}
	})
	setTimeout(() => {
		anim_items[random_item].classList.add(`_anim-icon-${arr[number]}`);
	}, 100);
}

if (document.querySelector('.icon-anim img')) {
	setInterval(() => {
		getRandomAnimate();
	}, 20000);
}

const btnHome = document.querySelector('.controls-slot__btn-home');
function getRandomAnimate2() {
	let number = getRandom(1, 3);

	if (btnHome.classList.contains('_anim-1')) {
		btnHome.classList.remove('_anim-1');
	} else if (btnHome.classList.contains('_anim-2')) {
		btnHome.classList.remove('_anim-2');
	}

	setTimeout(() => {
		btnHome.classList.add(`_anim-${number}`);
	}, 100);
}

if (btnHome) {
	setInterval(() => {
		getRandomAnimate2();
	}, 20000);
}

// Функция-предохранитель - если в банке будет очень много денег, уменьшает шрифт текста
function checkLengthScore() {
	const score = document.querySelector('.score-box__score');
	let length = score.clientWidth;
	if (length > 110) {
		if (score.classList.contains('_txt-middle')) score.classList.remove('_txt-middle');
		score.classList.add('_txt-small');
	}
	else if (length > 70 && length <= 110) {
		if (score.classList.contains('_txt-small')) score.classList.remove('_txt-small');
		score.classList.add('_txt-middle');
	}
	else {
		if (score.classList.contains('_txt-middle')) score.classList.remove('_txt-middle');
		if (score.classList.contains('_txt-small')) score.classList.remove('_txt-small');
	}
}

//========================================================================================================================================================
if (document.querySelector('.wrapper_game')) checkLengthScore();
export const configAboutGame = {
	stage: 0 // 0 - slider about active, 1 - single info about active
}

//========================================================================================================================================================
const configSlot = {
	currentWin: 0,
	winCoeff_1: 50,
	bet: 50
}
const configGSAP = {
	duration_1: 3.5
}

//game-1
if (document.querySelector('.slot__body')) {
	document.querySelector('.slot__body').classList.add('_active');
	document.querySelector('.score').textContent = sessionStorage.getItem('money');
}

function showWinScreen(status, count = 0) {
	const winCont = document.querySelector('.win-box__money');
	const win = document.querySelector('.win-box');
	const winStatus = document.querySelector('.win-box__status p');

	if (status) {
		winStatus.textContent = 'you win';
		winCont.textContent = count;
		win.classList.add('_visible');
	}
	else if (!status) {
		winStatus.textContent = 'you loose';
		winCont.textContent = count;
		win.classList.add('_visible');
	}
}

//========================================================================================================================================================
let slot1 = null;

class Slot1 {
	constructor(domElement, config = {}) {
		Symbol1.preload();

		this.currentSymbols = [
			["1", "2", "3"],
			["4", "5", "6"],
			["7", "8", "1"],
		];

		this.nextSymbols = [
			["1", "2", "3"],
			["4", "5", "6"],
			["7", "8", "1"],
		];

		this.container = domElement;

		this.reels = Array.from(this.container.getElementsByClassName("reel1")).map(
			(reelContainer, idx) =>
				new Reel1(reelContainer, idx, this.currentSymbols[idx])
		);

		this.spinButton = document.querySelector('.controls-slot__button-spin');
		this.spinButton.addEventListener("click", () => {
			//при запуске сбрасываем интервал запуска между слотами
			tl.to(this.spinButton, {});

			if ((+sessionStorage.getItem('money') >= configSlot.bet)) {
				this.spin();
			} else {
				noMoney('.score');
			}
		});

		this.maxBetButton = document.querySelector('.controls-slot__max-bet');
		this.maxBetButton.addEventListener("click", () => {
			//при запуске сбрасываем интервал запуска между слотами
			tl.to(this.spinButton, {});

			const money = +sessionStorage.getItem('money');
			let bet = 0;
			if (money > 1050) {
				bet = 1000;
				configSlot.bet = bet;
				this.spin();
			} else if (money < 1000 && money > 100) {
				bet = money - 100;
				configSlot.bet = bet;
				this.spin();
			} else {
				noMoney('.score');
			}
		});

		if (config.inverted) {
			this.container.classList.add("inverted");
		}
		this.config = config;
	}

	async spin() {
		this.currentSymbols = this.nextSymbols;
		this.nextSymbols = [
			[Symbol1.random(), Symbol1.random(), Symbol1.random()],
			[Symbol1.random(), Symbol1.random(), Symbol1.random()],
			[Symbol1.random(), Symbol1.random(), Symbol1.random()]
		];

		this.onSpinStart(this.nextSymbols);

		await Promise.all(
			this.reels.map((reel) => {
				reel.renderSymbols(this.nextSymbols[reel.idx]);
				return reel.spin(this.nextSymbols);
			})
		);
	}

	onSpinStart(symbols) {
		deleteMoney(configSlot.bet, '.score', 'money');

		this.spinButton.classList.add('_hold');
		this.maxBetButton.classList.add('_hold');

		this.config.onSpinStart?.(symbols);
	}

	onSpinEnd(symbols) {
		this.spinButton.classList.remove('_hold');
		this.maxBetButton.classList.remove('_hold');

		this.config.onSpinEnd?.(symbols);
		if (configSlot.bet !== 50)
			configSlot.bet = 50;
	}
}

class Reel1 {
	constructor(reelContainer, idx, initialSymbols) {
		this.reelContainer = reelContainer;
		this.idx = idx;

		this.symbolContainer = document.createElement("div");
		this.symbolContainer.classList.add("icons");
		this.reelContainer.appendChild(this.symbolContainer);

		initialSymbols.forEach((symbol) =>
			this.symbolContainer.appendChild(new Symbol1(symbol).img)
		);
	}

	get factor() {
		return 3 + Math.pow(this.idx / 2, 2);
	}

	renderSymbols(nextSymbols) {
		const fragment = document.createDocumentFragment();

		for (let i = 3; i < 3 + Math.floor(this.factor) * 10; i++) {
			const icon = new Symbol1(
				i >= 10 * Math.floor(this.factor) - 2
					? nextSymbols[i - Math.floor(this.factor) * 10]
					: undefined
			);
			fragment.appendChild(icon.img);
		}

		this.symbolContainer.appendChild(fragment);
	}

	async spin(symbols) {
		// запускаем анимацию смещения колонки
		this.param = ((Math.floor(this.factor) * 10) / (3 + Math.floor(this.factor) * 10)) * 100;

		await tl.fromTo(this.symbolContainer, { translateY: 0, }, {
			translateY: `${-this.param}%`,
			duration: configGSAP.duration_1,
			onComplete: () => {

				// определяем какое количество картинок хотим оставить в колонке
				const max = this.symbolContainer.children.length - 3; // 3 - количество картинок в одной колонке после остановки

				gsap.to(this.symbolContainer, { translateY: 0, duration: 0 });

				// запускаем цикл, в котором оставляем определенное количество картинок в конце колонки
				for (let i = 0; i < max; i++) {
					this.symbolContainer.firstChild.remove();
				}
			}
		}, '<10%');

		// После выполнения анимации запускаем сценарий разблокировки кнопок и проверки результата
		slot1.onSpinEnd(symbols);
	}
}

const cache1 = {};

class Symbol1 {
	constructor(name = Symbol1.random()) {
		this.name = name;

		if (cache1[name]) {
			this.img = cache1[name].cloneNode();
		} else {

			this.img = new Image();
			this.img.src = `img/game-1/slot-${name}.png`;

			cache1[name] = this.img;
		}
	}

	static preload() {
		Symbol1.symbols.forEach((symbol) => new Symbol1(symbol));
	}

	static get symbols() {
		return [
			'1',
			'2',
			'3',
			'4',
			'5',
			'6',
			'7',
			'8',
		];
	}

	static random() {
		return this.symbols[Math.floor(Math.random() * this.symbols.length)];
	}
}

const config1 = {
	inverted: false,
	onSpinStart: (symbols) => {
	},
	onSpinEnd: (symbols) => {
		if (symbols[0][0] == symbols[1][0] && symbols[1][0] == symbols[2][0] ||
			symbols[0][1] == symbols[1][1] && symbols[1][1] == symbols[2][1] ||
			symbols[0][2] == symbols[1][2] && symbols[1][2] == symbols[2][2]
		) {
			let currintWin = configSlot.bet * configSlot.winCoeff_1;

			// Записываем сколько выиграно на данный момент
			configSlot.currentWin += currintWin;
			addMoney(currintWin, '.score', 1000, 2000);
			checkLengthScore();

			showWinScreen(true, currintWin);
		} else {
			showWinScreen(false);
		}
	},
};

if (document.querySelector('.wrapper_game-1')) {
	slot1 = new Slot1(document.getElementById("slot1"), config1);
}

